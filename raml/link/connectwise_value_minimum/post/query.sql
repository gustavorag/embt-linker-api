insert
into cw_value_minimum
( agr_header_recid
, agr_detail_recid
, minimum_value
, created_by
)
values
( :agr_header_recid
, :agr_detail_recid
, :minimum_value
, :created_by
);
