INSERT INTO cw_value_minimum_snapshot (link_snapshot_id, cw_value_minimum_id, agr_header_recid, agr_detail_recid, minimum_value)
  SELECT :link_snapshot_id, cw_value_minimum_id, agr_header_recid, agr_detail_recid, minimum_value
  FROM cw_value_minimum;