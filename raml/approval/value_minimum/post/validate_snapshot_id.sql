SELECT IFNULL(MIN(link_snapshot_id), 0) AS cw_value_minimum_snapshot_id
FROM link_snapshot
WHERE link_snapshot_id = :link_snapshot_id AND snapshot_type = 'CW-VALUE-MINIMUM'