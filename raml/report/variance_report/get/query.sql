SELECT 
    sender_domain as zix_customer, 
    agr_detail_recid, 
    agd_amount as connetwise_amount,
    count as billing_amount, 
    coalesce(minimum_value, 0) as minimum_amount,
    coalesce((count - agd_amount),0) as billing_cw_variance,
    coalesce((minimum_value - agd_amount),0) as minimum_cw_variance
FROM postgresql.public.agr_detail
JOIN mysql.exmautobilling.cw_zix_usage_link as link USING (agr_detail_recid)
LEFT JOIN mysql.exmautobilling.cw_value_minimum as minv USING (agr_detail_recid)
JOIN mysql.exmautobilling.zix_usage usage USING (sender_domain)
ORDER BY agr_detail_recid 