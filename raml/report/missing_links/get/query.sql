SELECT 
    'Not linked' as zix_customer, 
    cast(coalesce(cwdetail.agr_detail_recid ,0 ) AS varchar(15)) as agr_detail_recid, 
    0 as billing_db_amount, 
    coalesce(cwdetail.agd_amount, 0) as cw_amount
FROM postgresql.public.agr_detail cwdetail
LEFT JOIN mysql.exmautobilling.cw_zix_usage_link as link ON cwdetail.agr_detail_recid = link.agr_detail_recid
WHERE link.agr_detail_recid IS NULL
UNION DISTINCT
SELECT 
    usage.sender_domain as zix_customer, 
    'Not Linked' as agr_detail_recid, 
    coalesce(usage.count,0) as billing_db_amount, 
    0 as cw_amount
FROM mysql.exmautobilling.zix_usage as usage 
LEFT JOIN mysql.exmautobilling.cw_zix_usage_link as link ON usage.sender_domain = link.sender_domain
WHERE link.sender_domain IS NULL
ORDER BY zix_customer, agr_detail_recid